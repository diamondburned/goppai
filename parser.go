package oppai

import (
	"bufio"
	"errors"
	"io"
	"strings"

	vector "github.com/atedja/go-vector"
	"github.com/davecgh/go-spew/spew"
)

var (
	// ErrUnsupportedGamemode is returned when a gamemode not standard is used
	ErrUnsupportedGamemode = errors.New("Unsupported gamemode")
)

/* ------------------------------------------------------------- */
/* beatmap parser                                                */

// Parser for storing the status of parsing
type Parser struct {
	lastline string // last lines touched
	Beatmap  *Map
	section  string
}

func trimSpace(s string) string { return strings.TrimSpace(s) }

func (p *Parser) property() []string {
	split := strings.SplitN(p.lastline, ":", 2)
	split[0] = trimSpace(split[0])
	if len(split) > 1 {
		split[1] = trimSpace(split[1])
	}
	return split
}

func (p *Parser) metadata() {
	pr := p.property()
	if len(pr) < 2 {
		return
	}

	switch pr[0] {
	case "Title":
		p.Beatmap.Title = pr[1]
	case "TitleUnicode":
		p.Beatmap.TitleUnicode = pr[1]
	case "Artist":
		p.Beatmap.Artist = pr[1]
	case "ArtistUnicode":
		p.Beatmap.ArtistUnicode = pr[1]
	case "Creator":
		p.Beatmap.Creator = pr[1]
	case "Version":
		p.Beatmap.Version = pr[1]
	case "BeatmapSetID":
		p.Beatmap.BeatmapsetID = pr[1]
	}
}

func (p *Parser) general() error {
	pr := p.property()
	if pr[0] == "Mode" {
		mode, err := parseInt(pr[1])
		if err != nil {
			return err
		}

		if mode != ModeStd {
			return ErrUnsupportedGamemode
		}

		p.Beatmap.Mode = mode
	}

	return nil
}

func (p *Parser) difficulty() error {
	pr := p.property()

	v, err := parseFloat(trimSpace(pr[1]))
	if err != nil {
		return err
	}

	switch pr[0] {
	case "CircleSize":
		p.Beatmap.CS = v
	case "OverallDifficulty":
		p.Beatmap.OD = v
	case "ApproachRate":
		p.Beatmap.AR = v
	case "HPDrainRate":
		p.Beatmap.HP = v
	case "SliderMultiplier":
		p.Beatmap.SV = v
	case "SliderTickRate":
		p.Beatmap.TickRate = v
	}

	return nil
}

func (p *Parser) timing() error {
	s := strings.Split(p.lastline, ",")

	if len(s) > 8 {
		info("timing point with trailing values")
	}

	time, err := parseDouble(s[0])
	if err != nil {
		return err
	}

	mspBeat, err := parseDouble(s[1])
	if err != nil {
		return err
	}

	t := Timing{
		Time:      time,
		MsPerBeat: mspBeat,
	}

	if len(s) >= 7 {
		t.Change = !(s[6] == "0")
	}

	p.Beatmap.TPoints = append(p.Beatmap.TPoints, &t)

	return nil
}

func (p *Parser) objects(s *ObjScanner) error {
	errors := make([]error, 4)

	var t0, t1, t2 float64
	var t3 int

	t0, errors[0] = parseDouble(string(s.GetField()))
	t1, errors[1] = parseDouble(string(s.GetField()))
	t2, errors[2] = parseDouble(string(s.GetField()))
	t3, errors[3] = parseInt(string(s.GetField()))

	for _, e := range errors {
		if e != nil {
			return e
		}
	}

	obj := HitObject{
		Time:    t2,
		Type:    t3,
		Strains: []float64{0.0, 0.0},
	}

	if (obj.Type & ObjCircle) != 0 {
		p.Beatmap.NCircles++
		obj.Data = Circle{
			pos: vector.NewWithValues([]float64{t0, t1}),
		}
	} else if (obj.Type & ObjSpinner) != 0 {
		p.Beatmap.NSpinners++
	} else if (obj.Type & ObjSlider) != 0 {
		s.GetField()
		s.GetField()
		p.Beatmap.NSliders++

		rep, err := parseInt(unsafeByteToStr(s.GetField()))
		if err != nil {
			return err
		}

		dist, err := parseDouble(unsafeByteToStr(s.GetField()))
		if err != nil {
			return err
		}

		obj.Data = Slider{
			pos:         vector.NewWithValues([]float64{t0, t1}),
			repetitions: rep,
			distance:    dist,
		}
	}

	p.Beatmap.Objects = append(p.Beatmap.Objects, &obj)

	return nil
}

// Map returns the beatmap info
func (p *Parser) Map(reader io.Reader) {
	var line string
	objScanner := &ObjScanner{}
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line = scanner.Text()
		p.lastline = line

		if strings.HasPrefix(line, " ") || strings.HasPrefix(line, "_") {
			continue
		}

		p.lastline = strings.TrimSpace(line)
		line = p.lastline
		if len(line) < 1 {
			continue
		}

		if strings.HasPrefix(line, "//") {
			continue
		}

		// [SectionName]
		if strings.HasPrefix(line, "[") {
			p.section = line[1 : len(line)-1]
			continue
		}

		switch p.section {
		case "Metadata":
			p.metadata()
		case "General":
			p.general()
		case "Difficulty":
			p.difficulty()
		case "TimingPoints":
			p.timing()
		case "HitObjects":
			objScanner.SetSource(scanner.Bytes())
			p.objects(objScanner)
		}
	}

	println(spew.Sdump(p.Beatmap.ArtistUnicode))

}
