# oppai5 (oppai-go)

osu!game pp calculator written in Go, forked form `github.com/flesnuk/oppai5`

## Credits

- Transpiled by hand version of [github.com/Francesco149/koohii](https://github.com/Francesco149/koohii) by [flesnuk](https://github.com/flesnuk)
- [Francesco149](https://github.com/Francesco149) for his help on the HD rework and `oppai`

## Improvements on the `master` branch

- Hidden rework updated
- Beatmap info missing "bug" fixed
