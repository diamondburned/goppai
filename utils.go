package oppai

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"
)

// Warnings ...
const Warnings = false

func info(s string) {
	if Warnings {
		fmt.Println(s)
	}
}

func unsafeByteToStr(b []byte) string {
	return string(b)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func roundOppai(x float64) int {
	return int(math.Floor((x) + 0.5))
}

func parseFloat(s string) (float32, error) {
	f, err := strconv.ParseFloat(s, 32)
	if err != nil {
		return 0, err
	}

	return float32(f), nil
}

func parseDouble(s string) (float64, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, err
	}

	return f, nil
}

func pow(a, b float64) float64 {
	if a == 1.52163 {
		return math.Exp(b * 0.4197821287029574)
	} else if a == 0.97 {
		return math.Exp(b * -0.030459207484708574)
	}

	return math.Exp(b * math.Log(a))
}

func clamp(num, min, max float64) float64 {
	if num > max {
		return max
	} else if num < min {
		return min
	}

	return num
}

func parseInt(s string) (int, error) {
	i, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return 0, err
	}

	return int(i), nil
}

func reverseSortFloat64s(x []float64) {
	sort.Float64s(x)
	n := len(x)
	for i := 0; i < n/2; i++ {
		j := n - i - 1
		x[i], x[j] = x[j], x[i]
	}

}

/* ------------------------------------------------------------- */
/* mods utils                                                    */

// BitMods contains all the bitwise enums for mods
// You could use Mods[<BitMods>] to return a short string
type BitMods uint32

// bitmask mods
const (
	None        BitMods = 0
	NoFail      BitMods = 1
	Easy        BitMods = 2
	TouchDevice BitMods = 4
	Hidden      BitMods = 8
	HardRock    BitMods = 16
	SuddenDeath BitMods = 32
	DoubleTime  BitMods = 64
	Relax       BitMods = 128
	HalfTime    BitMods = 256
	Nightcore   BitMods = 512 // Only set along with DoubleTime. i.e: NC only gives 576
	Flashlight  BitMods = 1024
	Autoplay    BitMods = 2048
	SpunOut     BitMods = 4096
	Relax2      BitMods = 8192  // Autopilot
	Perfect     BitMods = 16384 // Only set along with SuddenDeath. i.e: PF only gives 16416
	Key4        BitMods = 32768
	Key5        BitMods = 65536
	Key6        BitMods = 131072
	Key7        BitMods = 262144
	Key8        BitMods = 524288
	FadeIn      BitMods = 1048576
	Random      BitMods = 2097152
	Cinema      BitMods = 4194304
	Target      BitMods = 8388608
	Key9        BitMods = 16777216
	KeyCoop     BitMods = 33554432
	Key1        BitMods = 67108864
	Key3        BitMods = 134217728
	Key2        BitMods = 268435456
	ScoreV2     BitMods = 536870912
	LastMod     BitMods = 1073741824

	ModsSpeedChanging = DoubleTime | HalfTime | Nightcore
	ModsMapChanging   = HalfTime | Easy | ModsSpeedChanging
	KeyMod            = Key1 | Key2 | Key3 | Key4 | Key5 | Key6 | Key7 | Key8 | Key9 | KeyCoop
	FreeModAllowed    = NoFail | Easy | Hidden | HardRock | SuddenDeath | Flashlight | FadeIn | Relax | Relax2 | SpunOut | KeyMod
	ScoreIncreaseMods = Hidden | HardRock | DoubleTime | Flashlight | FadeIn
)

var (
	// Mods contains all the mods
	Mods = map[uint32]string{
		0:         "",
		1:         "NF",
		2:         "EZ",
		4:         "TD",
		8:         "HD",
		16:        "HR",
		32:        "SD",
		64:        "DT",
		128:       "RX",
		256:       "HT",
		512:       "NC",
		1024:      "FL",
		2048:      "Auto",
		4096:      "SO",
		8192:      "AP",
		16384:     "Perfect",
		32768:     "Key4",
		65536:     "Key5",
		131072:    "Key6",
		262144:    "Key7",
		524288:    "Key8",
		1048576:   "FadeIn",
		2097152:   "Random",
		4194304:   "LastMod",
		16777216:  "Key9",
		33554432:  "Key10",
		67108864:  "Key1",
		134217728: "Key3",
		268435456: "Key2",
	}
)

// IsInMod return true if Mod is in mods
func IsInMod(mods uint32, Mod ...BitMods) bool {
	for _, mod := range Mod {
		if mods&uint32(mod) != 0 {
			return true
		}
	}

	return false
}

// ModsStr returns a string representation of the mods, such as HDDT
func ModsStr(mods uint32) string {
	var modstring string

	switch mods {
	case 0:
		return ""
	default:
		for bit, str := range Mods {
			if (mods & bit) != 0 {
				modstring += str
			}
		}
	}

	return modstring
}

// StrMods returns the bitwise enum for the modstring
func StrMods(input string) uint32 {
	var enum uint32

	if strings.HasPrefix(input, "+") {
		input = input[1:]
	}

	for bit, str := range Mods {
		if strings.Contains(input, str) {
			enum += bit
		}
	}

	return enum
}
