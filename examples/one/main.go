package main

import (
	"bufio"
	"os"

	oppai "../../../oppai5"
	"github.com/davecgh/go-spew/spew"
)

func main() {
	reader := bufio.NewReader(os.Stdin)

	beatmap := oppai.Parse(reader)

	pp := oppai.PPInfo(beatmap, &oppai.Parameters{
		N300:  2215,
		N100:  8,
		Combo: 2724,
		Mods:  24,
	})

	spew.Dump(pp.Diff.Beatmap)
	spew.Dump(pp)
}
