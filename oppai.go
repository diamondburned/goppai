package oppai

import "io"

// Parameters for the PPInfo function
type Parameters struct {
	N300   int
	N100   int
	N50    int
	Misses uint
	Combo  int
	Mods   uint32
}

// PP info to return
type PP struct {
	PP     PPv2
	Stats  MapStats
	Diff   DiffCalc
	StepPP MultiAccPP
}

// PPInfo ..
func PPInfo(beatmap *Map, p *Parameters) (*PP, error) {
	var pp *PP
	var err error

	if p != nil {
		pp, err = getPP(
			beatmap,
			p.Mods,
			p.N300,
			p.N100,
			p.N50,
			int(p.Misses),
			int(p.Combo),
		)
	} else {
		pp, err = getPP(beatmap, 0, -1, 0, 0, 0, -1)
	}

	if err != nil {
		return nil, err
	}

	return pp, nil
}

func getPP(beatmap *Map, mods uint32, n300, n100, n50, nmiss, combo int) (*PP, error) {
	diff, err := (&DiffCalc{}).calcMapWithMods(*beatmap, mods)
	if err != nil {
		return nil, err
	}

	pp := &PPv2{}
	pp.PPv2WithMods(diff.Aim, diff.Speed, beatmap, mods, n300, n100, n50, nmiss, combo)

	diff.Beatmap.MaxCombo = beatmap.MaxCombo

	return &PP{
		PP:     *pp,
		Stats:  *diff.mapStats,
		Diff:   diff,
		StepPP: (&PPv2{}).PPv2StepWithMods(diff.Aim, diff.Speed, beatmap, mods),
	}, nil
}

// Parse parses the file f and returns a beatmap
func Parse(f io.Reader) *Map {
	parser := &Parser{}
	parser.Beatmap = &Map{}
	parser.Map(f)
	return parser.Beatmap
}
