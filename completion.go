package oppai

import (
	"errors"
	"log"
	"time"
)

var (
	// ErrObjectOutOfRange is returned when the failed object is miscalculated (should never happen)
	ErrObjectOutOfRange = errors.New("Object out of range")
)

type CompletionData struct {
	Progress time.Duration
	FullTime time.Duration
}

// Completion calculates the progress so far in the beatmap
func Completion(bm Map, pa Parameters) (*CompletionData, error) {
	failed := pa.N50 + pa.N100 + pa.N300 + int(pa.Misses) - 1

	log.Println("Comparing total objects vs so far:", len(bm.Objects), failed)

	if len(bm.Objects) < failed {
		return nil, ErrObjectOutOfRange
	}

	var (
		atObject   = bm.Objects[failed]
		lastObject = bm.Objects[len(bm.Objects)-1]
	)

	dura := time.Duration(atObject.Time * float64(time.Millisecond))
	done := time.Duration(lastObject.Time * float64(time.Millisecond))

	return &CompletionData{
		Progress: dura,
		FullTime: done,
	}, nil
}

// Percentage returns the percentage completion from data calculated
func (c *CompletionData) Percentage() float64 {
	dura := float64(c.Progress.Nanoseconds())
	done := float64(c.FullTime.Nanoseconds())

	return dura / done * 100.0
}
